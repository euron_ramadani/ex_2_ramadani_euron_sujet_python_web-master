-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 10 Mai 2020 à 16:36
-- Version du serveur :  5.6.20-log
-- Version de PHP :  5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `ramadani_euron_nettoyage_bd_104`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_adresse`
--

CREATE TABLE IF NOT EXISTS `t_adresse` (
`id_adresse` int(11) NOT NULL,
  `adresse` text NOT NULL,
  `ville` text NOT NULL,
  `NPA_adresse` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `t_adresse`
--

INSERT INTO `t_adresse` (`id_adresse`, `adresse`, `ville`, `NPA_adresse`) VALUES
(1, 'Route d''Echallens 41', 'Vuarrens', 1418),
(2, 'Rue du Maupas 51', 'Lausanne', 1004),
(3, 'Route de Boussens 19', 'Bettens', 1042),
(4, 'Chemin de la Tuiliere 5', 'Mont-sur-Lausanne', 1052);

-- --------------------------------------------------------

--
-- Structure de la table `t_adresse_batiment`
--

CREATE TABLE IF NOT EXISTS `t_adresse_batiment` (
`id_adresse_batiment` int(11) NOT NULL,
  `FK_adresse` int(11) NOT NULL,
  `FK_batiment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `t_batiment`
--

CREATE TABLE IF NOT EXISTS `t_batiment` (
`id_batiment` int(11) NOT NULL,
  `nom_batiment` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `t_batiment`
--

INSERT INTO `t_batiment` (`id_batiment`, `nom_batiment`) VALUES
(1, 'Helsana Assurances'),
(2, 'FCF Consulting Sarl'),
(3, 'ACF Services SA');

-- --------------------------------------------------------

--
-- Structure de la table `t_equipe`
--

CREATE TABLE IF NOT EXISTS `t_equipe` (
`id_equipe` int(11) NOT NULL,
  `nom_equipe` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `t_equipe`
--

INSERT INTO `t_equipe` (`id_equipe`, `nom_equipe`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'C');

-- --------------------------------------------------------

--
-- Structure de la table `t_equipe_batiment`
--

CREATE TABLE IF NOT EXISTS `t_equipe_batiment` (
`id_equipe_batiment` int(11) NOT NULL,
  `FK_equipe` int(11) NOT NULL,
  `FK_batiment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `t_equipe_materiel`
--

CREATE TABLE IF NOT EXISTS `t_equipe_materiel` (
`id_equipe_materiel` int(11) NOT NULL,
  `FK_equipe` int(11) NOT NULL,
  `FK_materiel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `t_equipe_telephone`
--

CREATE TABLE IF NOT EXISTS `t_equipe_telephone` (
`id_equipe_telephone` int(11) NOT NULL,
  `FK_telephone` int(11) NOT NULL,
  `FK_equipe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `t_mail`
--

CREATE TABLE IF NOT EXISTS `t_mail` (
`id_mail` int(11) NOT NULL,
  `adresse_mail` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `t_mail`
--

INSERT INTO `t_mail` (`id_mail`, `adresse_mail`) VALUES
(1, 'aditigifi-4213@yopmail.com'),
(2, 'zepatuhuf-1318@yopmail.com'),
(3, 'vyssallirrarro-2229@yopmail.com'),
(4, 'clowneriea');

-- --------------------------------------------------------

--
-- Structure de la table `t_materiel`
--

CREATE TABLE IF NOT EXISTS `t_materiel` (
`id_materiel` int(11) NOT NULL,
  `produit_materiel` text NOT NULL,
  `marque_materiel` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `t_materiel`
--

INSERT INTO `t_materiel` (`id_materiel`, `produit_materiel`, `marque_materiel`) VALUES
(1, 'Gants de protection', 'Reinhold'),
(2, 'Lavette', 'Spontex'),
(3, 'Pulvérisateur', 'Birchmeier'),
(4, 'Sel à neige', 'Taufix'),
(5, 'Autolaveuse', 'Nilfisk'),
(6, 'Nettoyage des fenêtres', ' Leifheit'),
(7, 'Solvant pour sol', 'IBS'),
(8, 'Lingettes', 'WYPALL X50');

-- --------------------------------------------------------

--
-- Structure de la table `t_personne`
--

CREATE TABLE IF NOT EXISTS `t_personne` (
`id_personne` int(11) NOT NULL,
  `nom_personne` text NOT NULL,
  `prenom_personne` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `t_personne`
--

INSERT INTO `t_personne` (`id_personne`, `nom_personne`, `prenom_personne`) VALUES
(1, 'Bernard', 'Martin'),
(2, 'Durand', 'Michele'),
(3, 'Petit', 'Renaud'),
(4, 'Bailly', 'Laurent'),
(5, 'op 1', '23'),
(6, 'op 1', '23'),
(7, 'op 1', '23'),
(8, 'op 1', '23'),
(9, 'op 1', '23');

-- --------------------------------------------------------

--
-- Structure de la table `t_personne_adresse`
--

CREATE TABLE IF NOT EXISTS `t_personne_adresse` (
`id_personne_adresse` int(11) NOT NULL,
  `FK_personne` int(11) NOT NULL,
  `FK_adresse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `t_personne_avoir_mail`
--

CREATE TABLE IF NOT EXISTS `t_personne_avoir_mail` (
`id_personne_avoir_mail` int(11) NOT NULL,
  `FK_personne` int(11) NOT NULL,
  `FK_mail` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `t_personne_avoir_mail`
--

INSERT INTO `t_personne_avoir_mail` (`id_personne_avoir_mail`, `FK_personne`, `FK_mail`) VALUES
(1, 2, 3),
(2, 2, 3),
(3, 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `t_personne_avoir_telephone`
--

CREATE TABLE IF NOT EXISTS `t_personne_avoir_telephone` (
`id_personne_avoir_telephone` int(11) NOT NULL,
  `FK_personne` int(11) NOT NULL,
  `FK_telephone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `t_personne_equipe`
--

CREATE TABLE IF NOT EXISTS `t_personne_equipe` (
`id_personne_equipe` int(11) NOT NULL,
  `FK_personne` int(11) NOT NULL,
  `FK_equipe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `t_telephone`
--

CREATE TABLE IF NOT EXISTS `t_telephone` (
`id_telephone` int(11) NOT NULL,
  `numero_telephone` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `t_telephone`
--

INSERT INTO `t_telephone` (`id_telephone`, `numero_telephone`) VALUES
(1, 789214532),
(2, 779437282),
(3, 798574326),
(4, 789281921);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_adresse`
--
ALTER TABLE `t_adresse`
 ADD PRIMARY KEY (`id_adresse`);

--
-- Index pour la table `t_adresse_batiment`
--
ALTER TABLE `t_adresse_batiment`
 ADD PRIMARY KEY (`id_adresse_batiment`), ADD KEY `FK_adresse` (`FK_adresse`,`FK_batiment`), ADD KEY `FK_adresse_2` (`FK_adresse`,`FK_batiment`), ADD KEY `FK_batiment` (`FK_batiment`);

--
-- Index pour la table `t_batiment`
--
ALTER TABLE `t_batiment`
 ADD PRIMARY KEY (`id_batiment`);

--
-- Index pour la table `t_equipe`
--
ALTER TABLE `t_equipe`
 ADD PRIMARY KEY (`id_equipe`);

--
-- Index pour la table `t_equipe_batiment`
--
ALTER TABLE `t_equipe_batiment`
 ADD PRIMARY KEY (`id_equipe_batiment`), ADD KEY `FK_equipe` (`FK_equipe`,`FK_batiment`), ADD KEY `FK_batiment` (`FK_batiment`);

--
-- Index pour la table `t_equipe_materiel`
--
ALTER TABLE `t_equipe_materiel`
 ADD PRIMARY KEY (`id_equipe_materiel`), ADD KEY `FK_equipe` (`FK_equipe`,`FK_materiel`), ADD KEY `FK_materiel` (`FK_materiel`);

--
-- Index pour la table `t_equipe_telephone`
--
ALTER TABLE `t_equipe_telephone`
 ADD PRIMARY KEY (`id_equipe_telephone`), ADD KEY `FK_telephone` (`FK_telephone`,`FK_equipe`), ADD KEY `FK_equipe` (`FK_equipe`);

--
-- Index pour la table `t_mail`
--
ALTER TABLE `t_mail`
 ADD PRIMARY KEY (`id_mail`);

--
-- Index pour la table `t_materiel`
--
ALTER TABLE `t_materiel`
 ADD PRIMARY KEY (`id_materiel`);

--
-- Index pour la table `t_personne`
--
ALTER TABLE `t_personne`
 ADD PRIMARY KEY (`id_personne`);

--
-- Index pour la table `t_personne_adresse`
--
ALTER TABLE `t_personne_adresse`
 ADD PRIMARY KEY (`id_personne_adresse`), ADD KEY `FK_personne` (`FK_personne`,`FK_adresse`), ADD KEY `FK_adresse` (`FK_adresse`);

--
-- Index pour la table `t_personne_avoir_mail`
--
ALTER TABLE `t_personne_avoir_mail`
 ADD PRIMARY KEY (`id_personne_avoir_mail`), ADD KEY `FK_personne` (`FK_personne`,`FK_mail`), ADD KEY `FK_mail` (`FK_mail`);

--
-- Index pour la table `t_personne_avoir_telephone`
--
ALTER TABLE `t_personne_avoir_telephone`
 ADD PRIMARY KEY (`id_personne_avoir_telephone`), ADD KEY `FK_personne` (`FK_personne`,`FK_telephone`), ADD KEY `FK_telephone` (`FK_telephone`);

--
-- Index pour la table `t_personne_equipe`
--
ALTER TABLE `t_personne_equipe`
 ADD PRIMARY KEY (`id_personne_equipe`), ADD KEY `FK_personne` (`FK_personne`,`FK_equipe`), ADD KEY `FK_equipe` (`FK_equipe`);

--
-- Index pour la table `t_telephone`
--
ALTER TABLE `t_telephone`
 ADD PRIMARY KEY (`id_telephone`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_adresse`
--
ALTER TABLE `t_adresse`
MODIFY `id_adresse` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `t_adresse_batiment`
--
ALTER TABLE `t_adresse_batiment`
MODIFY `id_adresse_batiment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_batiment`
--
ALTER TABLE `t_batiment`
MODIFY `id_batiment` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_equipe`
--
ALTER TABLE `t_equipe`
MODIFY `id_equipe` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_equipe_batiment`
--
ALTER TABLE `t_equipe_batiment`
MODIFY `id_equipe_batiment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_equipe_materiel`
--
ALTER TABLE `t_equipe_materiel`
MODIFY `id_equipe_materiel` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_equipe_telephone`
--
ALTER TABLE `t_equipe_telephone`
MODIFY `id_equipe_telephone` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_mail`
--
ALTER TABLE `t_mail`
MODIFY `id_mail` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `t_materiel`
--
ALTER TABLE `t_materiel`
MODIFY `id_materiel` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `t_personne`
--
ALTER TABLE `t_personne`
MODIFY `id_personne` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `t_personne_adresse`
--
ALTER TABLE `t_personne_adresse`
MODIFY `id_personne_adresse` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_personne_avoir_mail`
--
ALTER TABLE `t_personne_avoir_mail`
MODIFY `id_personne_avoir_mail` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_personne_avoir_telephone`
--
ALTER TABLE `t_personne_avoir_telephone`
MODIFY `id_personne_avoir_telephone` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_personne_equipe`
--
ALTER TABLE `t_personne_equipe`
MODIFY `id_personne_equipe` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_telephone`
--
ALTER TABLE `t_telephone`
MODIFY `id_telephone` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_adresse_batiment`
--
ALTER TABLE `t_adresse_batiment`
ADD CONSTRAINT `t_adresse_batiment_ibfk_1` FOREIGN KEY (`FK_adresse`) REFERENCES `t_adresse` (`id_adresse`),
ADD CONSTRAINT `t_adresse_batiment_ibfk_2` FOREIGN KEY (`FK_batiment`) REFERENCES `t_batiment` (`id_batiment`);

--
-- Contraintes pour la table `t_equipe_batiment`
--
ALTER TABLE `t_equipe_batiment`
ADD CONSTRAINT `t_equipe_batiment_ibfk_1` FOREIGN KEY (`FK_equipe`) REFERENCES `t_equipe` (`id_equipe`),
ADD CONSTRAINT `t_equipe_batiment_ibfk_2` FOREIGN KEY (`FK_batiment`) REFERENCES `t_batiment` (`id_batiment`);

--
-- Contraintes pour la table `t_equipe_materiel`
--
ALTER TABLE `t_equipe_materiel`
ADD CONSTRAINT `t_equipe_materiel_ibfk_1` FOREIGN KEY (`FK_equipe`) REFERENCES `t_equipe` (`id_equipe`),
ADD CONSTRAINT `t_equipe_materiel_ibfk_2` FOREIGN KEY (`FK_materiel`) REFERENCES `t_materiel` (`id_materiel`);

--
-- Contraintes pour la table `t_equipe_telephone`
--
ALTER TABLE `t_equipe_telephone`
ADD CONSTRAINT `t_equipe_telephone_ibfk_1` FOREIGN KEY (`FK_telephone`) REFERENCES `t_telephone` (`id_telephone`),
ADD CONSTRAINT `t_equipe_telephone_ibfk_2` FOREIGN KEY (`FK_equipe`) REFERENCES `t_equipe` (`id_equipe`);

--
-- Contraintes pour la table `t_personne_adresse`
--
ALTER TABLE `t_personne_adresse`
ADD CONSTRAINT `t_personne_adresse_ibfk_1` FOREIGN KEY (`FK_personne`) REFERENCES `t_personne` (`id_personne`),
ADD CONSTRAINT `t_personne_adresse_ibfk_2` FOREIGN KEY (`FK_adresse`) REFERENCES `t_adresse` (`id_adresse`);

--
-- Contraintes pour la table `t_personne_avoir_mail`
--
ALTER TABLE `t_personne_avoir_mail`
ADD CONSTRAINT `t_personne_avoir_mail_ibfk_1` FOREIGN KEY (`FK_personne`) REFERENCES `t_personne` (`id_personne`),
ADD CONSTRAINT `t_personne_avoir_mail_ibfk_2` FOREIGN KEY (`FK_mail`) REFERENCES `t_mail` (`id_mail`);

--
-- Contraintes pour la table `t_personne_avoir_telephone`
--
ALTER TABLE `t_personne_avoir_telephone`
ADD CONSTRAINT `t_personne_avoir_telephone_ibfk_1` FOREIGN KEY (`FK_personne`) REFERENCES `t_personne` (`id_personne`),
ADD CONSTRAINT `t_personne_avoir_telephone_ibfk_2` FOREIGN KEY (`FK_telephone`) REFERENCES `t_telephone` (`id_telephone`);

--
-- Contraintes pour la table `t_personne_equipe`
--
ALTER TABLE `t_personne_equipe`
ADD CONSTRAINT `t_personne_equipe_ibfk_1` FOREIGN KEY (`FK_personne`) REFERENCES `t_personne` (`id_personne`),
ADD CONSTRAINT `t_personne_equipe_ibfk_2` FOREIGN KEY (`FK_equipe`) REFERENCES `t_equipe` (`id_equipe`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
