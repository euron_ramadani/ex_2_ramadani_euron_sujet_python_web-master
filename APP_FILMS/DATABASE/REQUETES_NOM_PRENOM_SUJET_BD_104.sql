/*
	Toutes les colonnes
*/
SELECT * FROM t_adresse_batiment AS T1
INNER JOIN t_adresse AS T2 ON T2.id_adresse = T1.fk_adresse
INNER JOIN t_batiment AS T3 ON T3.id_batiment = T1.fk_batiment

/*
	Seulement certaines colonnes
*/
SELECT id_batiment, nom_batiment , id_adresse, adresse FROM t_adresse_batiment AS T1
INNER JOIN t_adresse AS T2 ON T2.id_adresse = T1.fk_adresse
INNER JOIN t_batiment AS T3 ON T3.id_batiment = T1.fk_batiment

/* 	
	Permet d'aficher toutes les lignes de la table de droite (t_batiment) (qui est écrite en sql à droite de t_adresse_batiment)
	y compris les lignes qui ne sont pas attribuées à des films.
*/
SELECT id_batiment, nom_batiment , id_adresse, adresse FROM t_adresse_batiment AS T1
INNER JOIN t_adresse AS T2 ON T2.id_adresse = T1.fk_adresse
RIGHT JOIN t_batiment AS T3 ON T3.id_batiment = T1.fk_batiment

/* 	
	Permet d'aficher toutes les lignes de la table de droite (t_batiment) (qui est écrite en sql à droite de t_adresse_batiment)
	y compris les lignes qui ne sont pas attribuées à des films.
*/
SELECT id_batiment, nom_batiment , id_adresse, adresse  FROM t_adresse_batiment AS T1
RIGHT JOIN t_adresse AS T2 ON T2.id_adresse = T1.fk_adresse
LEFT JOIN t_batiment AS T3 ON T3.id_batiment = T1.fk_batiment


/*
	Affiche TOUS les films qui n'ont pas de genre attribués
*/
SELECT id_batiment, nom_batiment , id_adresse, adresse  FROM t_adresse_batiment AS T1
RIGHT JOIN t_adresse AS T2 ON T2.id_adresse = T1.fk_adresse
LEFT JOIN t_batiment AS T3 ON T3.id_batiment = T1.fk_batiment


/*
	Affiche SEULEMENT les films qui n'ont pas de genre attribués
*/

SELECT id_batiment, nom_batiment , id_adresse, adresse  FROM t_adresse_batiment AS T1
RIGHT JOIN t_adresse AS T2 ON T2.id_adresse = T1.fk_adresse
LEFT JOIN t_batiment AS T3 ON T3.id_batiment = T1.fk_batiment
WHERE T1.fk_batiment IS NULL